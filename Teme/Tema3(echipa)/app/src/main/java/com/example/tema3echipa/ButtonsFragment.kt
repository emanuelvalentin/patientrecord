package com.example.tema3echipa

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_buttons.*
import java.util.*


class ButtonsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(com.example.tema3echipa.R.layout.fragment_buttons, container, false)
    }
    var alarmtime=Calendar.getInstance();

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        databtn.setOnClickListener {
            val inflater =
                this.layoutInflater

            val popupView: View = inflater.inflate(com.example.tema3echipa.R.layout.datapickerpopup, null)

            // create the popup window

            // create the popup window
            val width = LinearLayout.LayoutParams.WRAP_CONTENT
            val height = LinearLayout.LayoutParams.WRAP_CONTENT
            val focusable = true // lets taps outside the popup also dismiss it

            val popupWindow = PopupWindow(popupView, width, height, focusable)

            // show the popup window
            // which view you pass in doesn't matter, it is only used for the window tolken

            // show the popup window
            // which view you pass in doesn't matter, it is only used for the window tolken
            popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
            var datapiker=popupView.findViewById<DatePicker>(com.example.tema3echipa.R.id.timePicker);
            // dismiss the popup window when touched
            var ok=popupView.findViewById<Button>(com.example.tema3echipa.R.id.okbtn);
            ok.setOnClickListener {  popupWindow.dismiss()
                alarmtime.set(datapiker.year,datapiker.month,datapiker.dayOfMonth)
                datatext.text=datapiker.year.toString()+" "+datapiker.month.toString() +" "+datapiker.dayOfMonth.toString()
            }
            // dismiss the popup window when touched
            popupView.setOnTouchListener { v, event ->
                popupWindow.dismiss()
                alarmtime.set(datapiker.year,datapiker.month,datapiker.dayOfMonth)
                datatext.text=datapiker.year.toString()+" "+datapiker.month.toString() +" "+datapiker.dayOfMonth.toString()

                true
            }
        }
        timemebtn.setOnClickListener {

            val inflater =
                this.layoutInflater

            val popupView: View =
                inflater.inflate(com.example.tema3echipa.R.layout.timepikerpopup, null)

            // create the popup window

            // create the popup window
            val width = LinearLayout.LayoutParams.WRAP_CONTENT
            val height = LinearLayout.LayoutParams.WRAP_CONTENT
            val focusable = true // lets taps outside the popup also dismiss it

            val popupWindow = PopupWindow(popupView, width, height, focusable)
            popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)

            // show the popup window
            // which view you pass in doesn't matter, it is only used for the window tolken

            // show the popup window
            // which view you pass in doesn't matter, it is only used for the window tolken
            popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)

            // dismiss the popup window when touched
            var timepiker =
                popupView.findViewById<TimePicker>(com.example.tema3echipa.R.id.timePicker);
            // dismiss the popup window when touched
            var ok = popupView.findViewById<Button>(com.example.tema3echipa.R.id.okbtn);
            ok.setOnClickListener {
                popupWindow.dismiss()
                alarmtime.set(Calendar.HOUR_OF_DAY, timepiker.hour)
                alarmtime.set(Calendar.HOUR, timepiker.hour)
                alarmtime.set(Calendar.MINUTE,timepiker.minute);
                timetext.text=alarmtime.get(Calendar.HOUR_OF_DAY).toString()+" "+alarmtime.get(Calendar.MINUTE).toString()
            }
            // dismiss the popup window when touched
            popupView.setOnTouchListener { v, event ->
                popupWindow.dismiss()
                alarmtime.set(Calendar.HOUR_OF_DAY, timepiker.hour)
                alarmtime.set(Calendar.HOUR, timepiker.hour)
                alarmtime.set(Calendar.MINUTE,timepiker.minute);
                timetext.text=alarmtime.get(Calendar.HOUR_OF_DAY).toString()+" "+alarmtime.get(Calendar.MINUTE).toString()
                true
            }
        }
        setalarmbtn.setOnClickListener {

            val intent = Intent(activity, AlertReceiver::class.java)
            val pendingIntent = PendingIntent.getBroadcast(activity, 1, intent, 0)


            var alarm= activity?.getSystemService(ALARM_SERVICE) as AlarmManager;
alarm!!.setExact(AlarmManager.RTC,alarmtime.timeInMillis,pendingIntent)
}

    }
    companion object {
        @JvmStatic
        fun newInstance() = ButtonsFragment()
    }
}
