package com.example.tema2.appdatabase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.tema2.daos.UserDao
import com.example.tema2.models.User

@Database(entities = [User::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}
