package com.example.tema2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragment()
    }

    private fun addFragment()
    {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragments_container, ManageUsersFragment.newInstance())
        transaction.commit()
    }
}
