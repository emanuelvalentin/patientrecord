package com.example.tema2.adapters

import android.R.attr.data
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tema2.R
import com.example.tema2.models.User


class UserViewAdapter(private var users : List<User>?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class UserViewHolder(val view : View) : RecyclerView.ViewHolder(view){
        val textView = view.findViewById<TextView>(R.id.user_textView)
    }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{

                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.user_text_view, parent, false)

            return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UserViewHolder).textView.text = users?.get(position)?.toString()
    }

    override fun getItemCount(): Int {
        if(users != null){
            return users!!.size
        }
        return 0
    }

    fun setData(newUsers: List<User>) {
        users = newUsers
        notifyDataSetChanged()
    }

}