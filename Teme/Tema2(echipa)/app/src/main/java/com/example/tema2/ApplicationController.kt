package com.example.tema2

import android.app.Application
import androidx.room.Room
import com.example.tema2.appdatabase.AppDatabase

class ApplicationController : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this

        appDatabase = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "db_tema2"
        ).build()
    }

    companion object {
        var instance: ApplicationController? = null
            private set

        var appDatabase: AppDatabase? = null
            private set

    }
}