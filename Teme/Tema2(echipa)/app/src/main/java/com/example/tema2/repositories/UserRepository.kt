package com.example.tema2.repositories

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.tema2.ApplicationController
import com.example.tema2.appdatabase.AppDatabase
import com.example.tema2.models.User
import kotlin.concurrent.thread

class UserRepository(context: Context?) {
    private val appDatabase: AppDatabase = ApplicationController.appDatabase!!

    fun insertTask(vararg users: User) {
        InsertTask().execute(*users)
    }

    fun getUserByName(
        firstName: String?,
        lastName: String?
    ): User {
        return appDatabase.userDao().findByName(firstName!!, lastName!!)
    }

    fun Update(
        vararg users: User
    ){

        InsertTask().execute(*users)

    }


    fun deleteTask(user : User){
        DeleteUserTask().execute(user)
    }

    fun getUserByNameTask(
        firstName: String?,
        lastName: String?
    ): User? {
        return FindUserTask().execute(firstName, lastName).get()
    }

    fun getAllUsers() : LiveData<List<User>> {
        return appDatabase.userDao().getAll()
    }
    @SuppressLint("StaticFieldLeak")
    private inner class UpdateTask() :
        AsyncTask<User, Void?, Void?>() {

        override fun doInBackground(vararg users: User): Void? {
            appDatabase.userDao().Update(*users)
            return null
        }
    }

    @SuppressLint("StaticFieldLeak")
    private inner class InsertTask() :
        AsyncTask<User, Void?, Void?>() {

        override fun doInBackground(vararg users: User): Void? {
            appDatabase.userDao().insertAll(*users)
            return null
        }

    }




    @SuppressLint("StaticFieldLeak")
    private inner class DeleteUserTask() :
        AsyncTask<User, Void?, Void?>() {

        override fun doInBackground(vararg users: User): Void? {
            val user = users.firstOrNull()

            if (user != null) {
                appDatabase.userDao().delete(user)
            }

            return null
        }
    }
    @SuppressLint("StaticFieldLeak")
    private inner class FindUserTask() :
        AsyncTask<String, User?, User?>() {

        override fun doInBackground(vararg params: String): User? {
            return appDatabase.userDao().findByName(params[0], params[1])
        }

    }


}

interface OnUserRepositoryActionListener {
    fun actionSuccess()
    fun actionFailed()
}