package com.example.tema2

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tema2.adapters.UserViewAdapter
import com.example.tema2.models.User
import com.example.tema2.repositories.UserRepository
import kotlinx.android.synthetic.main.fragment_manage_users.*
import org.json.JSONArray
import java.net.URL
import java.util.*
import kotlin.concurrent.thread


class ManageUsersFragment : Fragment() {
    private val userRepo = UserRepository(context)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_manage_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val viewManager = LinearLayoutManager(activity)
        val viewAdapter = UserViewAdapter(userRepo.getAllUsers().value)

        recycle_users_list.layoutManager = viewManager
        recycle_users_list.adapter = viewAdapter

        val usersObservers= Observer<List<User>>{newUsersList ->
            viewAdapter.setData(newUsersList)
        }

        userRepo.getAllUsers().observe(this, usersObservers)
        btn_add_user.setOnClickListener {

            val firstName = edit_first_name.text.toString()
            val lastName = edit_last_name.text.toString()

            if(firstName.isNotEmpty() && lastName.isNotEmpty()){

                userRepo.insertTask(User(firstName= firstName, lastName = lastName))
            }


        }
        btn_remove_user.setOnClickListener {
            val firstName = edit_first_name.text.toString()
            val lastName = edit_last_name.text.toString()

            if(firstName.isNotEmpty() && lastName.isNotEmpty()){
                val user = userRepo.getUserByNameTask(firstName, lastName)

                if(user != null) {
                    userRepo.deleteTask(user)
                }
            }
        }
        btn_load_database.setOnClickListener {
            thread {

                    var url = URL("https://jsonplaceholder.typicode.com/users");
                    var conn = url.openConnection();
                    val input: Scanner = Scanner(conn.getInputStream())
                    var out = "";
                    while (input.hasNext()) {
                        out += input.next();
                    }

                    var array = JSONArray(out);
                    for (index in 0..array.length() - 1) {
                        var obj = array.getJSONObject(index)
                        var uid = obj.get("id") as Int;
                        var name = obj.get("name") as String;
                        var lastname = obj.get("username") as String;

                            userRepo.Update(User(uid, name, lastname))

                    }

            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): ManageUsersFragment = ManageUsersFragment()
    }
}
