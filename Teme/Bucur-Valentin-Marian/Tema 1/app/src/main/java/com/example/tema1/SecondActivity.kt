package com.example.tema1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*
import kotlinx.android.synthetic.main.fragment_first_second_activity.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        if ( fragments_container != null) {

            if (savedInstanceState != null) {
                return
            }
            val firstFragment = FirstFragmentSecondActivity.newInstance()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.fragments_container, firstFragment, "F1_A2")
            transaction.commit()
        }
    }
}
