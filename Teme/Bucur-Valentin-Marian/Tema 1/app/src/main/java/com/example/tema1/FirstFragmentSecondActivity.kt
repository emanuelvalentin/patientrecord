package com.example.tema1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_first_second_activity.*

class FirstFragmentSecondActivity : Fragment() {

    companion object {
        fun newInstance() : FirstFragmentSecondActivity = FirstFragmentSecondActivity()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first_second_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_add_f2_a2.setOnClickListener{
            if(fragmentManager?.fragments?.size==1) {
                val secondFragment = SecondFragmentSecondActivity.newInstance()
                val transaction = fragmentManager?.beginTransaction()
                transaction?.add(R.id.fragments_container, secondFragment)
                transaction?.commit()
            }
        }
    }
}
