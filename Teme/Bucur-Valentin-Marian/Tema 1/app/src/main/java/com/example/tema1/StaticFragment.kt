package com.example.tema1

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_static.*
import kotlinx.android.synthetic.main.fragment_static.view.*

class StaticFragment : Fragment() {

    companion object {
        fun newInstance() : StaticFragment = StaticFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_static, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        first_btn.setOnClickListener{
            val intent = Intent(this.context, SecondActivity::class.java)
            startActivity(intent)
            activity?.finish();
        }

    }

}
