package com.example.tema1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_second.*
import kotlinx.android.synthetic.main.fragment_first_second_activity.*
import kotlinx.android.synthetic.main.fragment_second_second_activity.*

class SecondFragmentSecondActivity : Fragment() {

    companion object {
        fun newInstance() : SecondFragmentSecondActivity = SecondFragmentSecondActivity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_second_second_activity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_replace_f2a2_f3a2.setOnClickListener {
            val thirdFragment = ThirdFragmentSecondActivity.newInstance()
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.fragments_container, thirdFragment)
            transaction?.commit()
        }

        btn_remove_f1a2.setOnClickListener {
            val transaction = fragmentManager?.beginTransaction()
            val fragment = fragmentManager?.findFragmentByTag("F1_A2")
            if(fragment != null) {
                transaction?.remove(fragment)
                transaction?.commit()
            }

            btn_close_activity.setOnClickListener {
                activity?.finish()
            }
        }
    }


}
