package com.example.tema1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_a1.*
import kotlinx.android.synthetic.main.activity_a2.*

class A2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_a2)

       var man=supportFragmentManager
       var tranz= man.beginTransaction()
        tranz.add(A2fragmentplace.id,F1A2())
        tranz.commit()

    }
}
