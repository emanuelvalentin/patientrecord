﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class server
    {
        Socket lisener;
        Client[] clientlist = new Client[100];
        int port;
      
        private void UDPStart()
        {
            Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPAddress adress = IPAddress.Any;
            IPEndPoint endpoint = new IPEndPoint(adress, 11511);
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint senderRemote = (EndPoint)sender;
            
            sock.Bind(endpoint);
            byte[] s = new byte[1024];
          
            while (true)
            {
                sock.ReceiveFrom(s, ref senderRemote);
               
                for (int i = 0; i < 10; i++)
                {
                    sock.SendTo(Encoding.UTF8.GetBytes(port.ToString()), senderRemote);
                    
                    Thread.Sleep(100);
                }
               
            }

        }
        
        public void start()

        {
            
            port = Math.Abs((int)DateTime.Now.Ticks) % (65536-10000)+10000;
            Thread tread = new Thread(new ThreadStart(UDPStart));
            tread.Start();
            IPAddress adress = IPAddress.Any;
            IPEndPoint endpoint=new IPEndPoint(adress,port);
            for (int i = 0; i < clientlist.Length; i++)
                clientlist[i] = new Client();
            lisener = new Socket(adress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                lisener.Bind(endpoint);
                lisener.Listen(100);
                while (true)
                {
                    Socket clientsock = lisener.Accept();
                    while (clientsock !=null)
                    {
                        foreach (Client cl in clientlist)
                        {
                            if (cl.finish())
                            {
                                cl.start(clientsock);
                                clientsock = null;
                                break;
                            }
                        }
                        Thread.Sleep(1);
                    }
                }
            }
            catch (Exception e)
            { }
        }
    }
}
