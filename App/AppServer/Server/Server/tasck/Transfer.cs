﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.tasck
{
    class Transfer
    {
        
        struct data
        {
           public  Int64 transfer_time { get; set; }
           public  Int64 transfer_id { get; set; }
        }
        public string transfer(string s, MySqlConnection connection, string Uid, string fid) {

            data info=new data();
            info.transfer_id = 0;
            info.transfer_time = DateTime.Now.Ticks;
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(2);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(JsonConvert.SerializeObject(info), eccLevel))
                {
                    using (Base64QRCode qrCode = new Base64QRCode(qrCodeData))
                    {

                        return qrCode.GetGraphic(10);
                       
                    }
                }
            }
            
        }
        public string transfer_from(string s, MySqlConnection connection, string Uid)
        {
            return "";
        }
    }
}
