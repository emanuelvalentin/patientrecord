﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Print
{
    public class Page
    {
        dynamic data;
        int pagenr = 1;
        Font title = new Font("Arial", 25);
        Font normal = new Font("Arial", 10);
        Font subtitle = new Font("Arial", 10, FontStyle.Bold);
        Font subtitle1 = new Font("arial", 9f, FontStyle.Bold | FontStyle.Underline | FontStyle.Italic);
        Brush fill = new SolidBrush(Color.FromArgb(15, 0, 0, 0));
        Font checkboxfont = new Font("Arial", 9.25f);
        public Page(dynamic data)
        {
            this.data = data;
        }
        public void nextpage(PrintPageEventArgs ev)
        {
            ev.HasMorePages = true;
            switch (pagenr)
            {
                default: { ev.HasMorePages = false; break; };
                case 1:
                    {
                        page1(ev.Graphics, ev.MarginBounds);

                        break;
                    };
                case 2:
                    {
                        page2(ev.Graphics, ev.MarginBounds);
                        break;
                    }
            };
            pagenr++;
        }
        private void page1(Graphics g, Rectangle bound)
        {
            UInt64 fisanr = (UInt64)DateTime.Now.Ticks;//de schimbat
            QRCodeGenerator.ECCLevel eccLevel = (QRCodeGenerator.ECCLevel)(2);
            using (QRCodeGenerator qrGenerator = new QRCodeGenerator())
            {
                using (QRCodeData qrCodeData = qrGenerator.CreateQrCode(fisanr.ToString(), eccLevel))
                {
                    using (QRCode qrCode = new QRCode(qrCodeData))
                    {

                        Bitmap image = qrCode.GetGraphic(5);
                        g.DrawImage(image, (bound.X * 2 + bound.Width) - 135, -20, 150, 150);
                    }
                }
            }

            String data = "00-00-0000",//de schimbat

                numedoctor = "Nume Prenume" //de schimbat

                , checkboxempty = "☐", checkboxchecked = "☑";
            string preluat_de = numedoctor.Equals(string.Empty) ? checkboxempty + "10-Preluat de:" : checkboxchecked + "10-Preluat de:";

            string ora_preluarii = "00:00";//de scimbat
            float plus_height = g.MeasureString("D", checkboxfont).Height;

            float x = bound.X / 2, y = bound.Y / 2, width = bound.X * 1.5f + bound.Width, height = bound.Y * 1.5f + bound.Height;
            {
                g.DrawString("FIŞA CPU", title, Brushes.Black, x + width / 2 - g.MeasureString("Fisa CPU", title).Width / 2, y);
                y = y + g.MeasureString("G", title).Height;
                g.DrawString("SPITAL", normal, Brushes.Black, x, y);
                y += g.MeasureString("G", normal).Height;
                g.DrawLine(Pens.Black, x, y, width, y);
                g.DrawLine(Pens.Black, x, y, x, height);
                g.DrawLine(Pens.Black, width, y, width, height);
                g.DrawLine(Pens.Black, x, height, width, height);
                g.DrawString(pagenr.ToString(), normal, Brushes.Black, width / 2 - g.MeasureString(pagenr.ToString(), normal).Width / 2, height);
                g.DrawString("UNITATEA PRIMIRI URGENŢE", subtitle, Brushes.Black, x, y + 1);
                g.DrawString("PACIENT", subtitle, Brushes.Black, x + 280, y + 1);
                g.DrawString("STARE PACIENT", subtitle, Brushes.Black, width - g.MeasureString("STARE PACIENT", subtitle).Width, y + 1);
                g.FillRectangle(fill, x, y, width - x, g.MeasureString("G", subtitle).Height);
                y += plus_height;
                g.DrawLine(Pens.Black, x + 275, y, x + 275, y + 18 * g.MeasureString("G", checkboxfont).Height);
                g.DrawLine(Pens.Black, x + 600, y, x + 600, y + (18 * g.MeasureString("G", checkboxfont).Height));
                g.DrawLine(Pens.Black, x, y, width, y);
                g.DrawLine(Pens.Black, x, y + 6 * plus_height, width, y + 6 * plus_height);
                g.DrawLine(Pens.Black, x, y + 11 * plus_height, width, y + 11 * plus_height);
                g.DrawLine(Pens.Black, x, y + 18 * plus_height, width, y + 18 * plus_height);
                g.FillRectangle(fill, x, y + 18 * plus_height, width - x, g.MeasureString("G", subtitle).Height);
                g.DrawLine(Pens.Black, x, y + 19 * plus_height, width, y + 19 * plus_height);

            }
            float pozy = y, pozx = x;
            {
                g.DrawString("NR fişă :", subtitle, Brushes.Black, x, y);
                g.DrawString("Data:", subtitle, Brushes.Black, x + 275 - g.MeasureString("Data:", subtitle).Width, y);
                y += plus_height;

                g.DrawString(fisanr.ToString(), checkboxfont, Brushes.Black,
                    x + g.MeasureString("9223372036854775807", checkboxfont).Width / 2 - g.MeasureString(fisanr.ToString(), checkboxfont).Width / 2, y);
                g.DrawString(data, checkboxfont, Brushes.Black, x + 275 - g.MeasureString(data, checkboxfont).Width, y);
                y += plus_height;
                g.DrawString(preluat_de, checkboxfont, Brushes.Black, x, y);
                g.DrawString("La ora:", checkboxfont, Brushes.Black, x + 275 - g.MeasureString("La ora:", checkboxfont).Width, y);
                y += plus_height;
                g.DrawString(numedoctor, checkboxfont, Brushes.Black, x, y);
                g.DrawString(ora_preluarii, checkboxfont, Brushes.Black, x + 275 - g.MeasureString(ora_preluarii, checkboxfont).Width, y);//inlocuit
                y += plus_height;
                g.DrawString("☐11-Ora prim consult medical", checkboxfont, Brushes.Black, x, y);
                g.DrawString(ora_preluarii, checkboxfont, Brushes.Black, x + 275 - g.MeasureString(ora_preluarii, checkboxfont).Width, y);//inlocuit
                y += plus_height;
                g.DrawString("☐12-Grupa sanguina", checkboxfont, Brushes.Black, x, y);
                g.DrawString("AA", checkboxfont, Brushes.Black, x + 10 + g.MeasureString("12 - Grupa sanguina", checkboxfont).Width, y);
                y += plus_height;
                g.DrawString("  Motivul prezentarii:", subtitle1, Brushes.Black, x, y);
                y = 19 * plus_height;

                g.DrawString("Adus de:", subtitle1, Brushes.Black, x, y);
                g.DrawString("Adus de la:", subtitle1, Brushes.Black, x + 160, y);
                y += plus_height;
                g.DrawString("☐13-SAJ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐17-Domiciliu", checkboxfont, Brushes.Black, x + 160, y);
                y += plus_height;
                g.DrawString("☐14-SMURD", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐18-Unitate sanit.", checkboxfont, Brushes.Black, x + 160, y);
                y += plus_height;
                g.DrawString("☐15-Mijloace proprii", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐19-Loc public", checkboxfont, Brushes.Black, x + 160, y);
                y += plus_height;
                g.DrawString("☐16-Alt", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐20-Loc muncă", checkboxfont, Brushes.Black, x + 160, y);
                y += plus_height;
                g.DrawString("☐13-Fisa nr:", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐21-Alt:", checkboxfont, Brushes.Black, x + 160, y);
                y += plus_height;
                g.DrawString(fisanr.ToString(), checkboxfont, Brushes.Black, x, y);
                g.DrawString("Scoala", checkboxfont, Brushes.Black, x + 160, y);
            }
            y = pozy; x += 275;
            {
                y += plus_height / 2;
                g.DrawString("Nume:  ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Popovici", checkboxfont, Brushes.Black,
                    x + 70 + ((600 - x) / 2)
                    - g.MeasureString("Popovici", checkboxfont).Width / 2, y);
                y += (plus_height + plus_height / 2);
                g.DrawString("Prenume:  ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Popovici", checkboxfont, Brushes.Black,
                    x + 70 + ((600 - x) / 2)
                    - g.MeasureString("Popovici", checkboxfont).Width / 2, y);
                y += (plus_height + plus_height / 2);
                g.DrawString("Varsta: ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("000", checkboxfont, Brushes.Black, x + 50, y);
                g.DrawString("Data nasterii: ", checkboxfont, Brushes.Black, x + 100, y);
                g.DrawString("00-00-0000", checkboxfont, Brushes.Black, x + 170, y);
                g.DrawString("☐M", checkboxfont, Brushes.Black, pozx + 600 - 5 - g.MeasureString("☐M", checkboxfont).Width, y);
                y += (plus_height + plus_height / 4);
                g.DrawString("CNP: ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("12345678901234567890", checkboxfont, Brushes.Black, x + 50, y);
                g.DrawString("☐F", checkboxfont, Brushes.Black, pozx + 600 - 5 - g.MeasureString("☐M", checkboxfont).Width, y);
            }
            y = pozy + plus_height * 6;
            {
                float aux = 0;
                y += plus_height * 1 / 4;
                g.DrawString("Domiciliu:  ", subtitle1, Brushes.Black, x, y);
                g.DrawString(" Judet ", checkboxfont, Brushes.Black, aux = x + g.MeasureString("Domiciliu:  ", subtitle1).Width, y);
                g.DrawString("AA", checkboxfont, Brushes.Black, aux = aux + g.MeasureString(" Judet ", checkboxfont).Width, y);
                g.DrawString("Telefon:", checkboxfont, Brushes.Black, aux = aux + g.MeasureString(" AA ", checkboxfont).Width, y);
                g.DrawString("0123456789012345", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("Telefon: ", checkboxfont).Width, y);
                y += plus_height * 5 / 4;
                g.DrawString("Localitate: ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Onesti ", checkboxfont, Brushes.Black, x + g.MeasureString("Localitate : ", checkboxfont).Width, y);
                y += plus_height * 5 / 4;
                g.DrawString("Strada: ", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Oituz ", checkboxfont, Brushes.Black, x + g.MeasureString("Localitate : ", checkboxfont).Width, y);
                y += plus_height * 5 / 4;
                g.DrawString("Nr.: ", checkboxfont, Brushes.Black, aux = x, y);
                g.DrawString("10000 ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("nr.: ", checkboxfont).Width, y);
                g.DrawString("Bl.: ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("10000 ", checkboxfont).Width, y);
                g.DrawString("10000 ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("nr.: ", checkboxfont).Width, y);
                g.DrawString("Sc.: ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("10000 ", checkboxfont).Width, y);
                g.DrawString("10000 ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("nr.: ", checkboxfont).Width, y);
                g.DrawString("Et.: ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("10000 ", checkboxfont).Width, y);
                g.DrawString("10000 ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("nr.: ", checkboxfont).Width, y);
                g.DrawString("Ap.: ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("10000 ", checkboxfont).Width, y);
                g.DrawString("10000 ", checkboxfont, Brushes.Black, aux = aux + g.MeasureString("nr.: ", checkboxfont).Width, y);
            }
            y = pozy + plus_height * 11;
            {
                g.DrawString("Functii vitale la preluare:", subtitle1, Brushes.Black, x, y);
                y += plus_height;
                g.DrawString("☐35-Decedat", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐39-Resuscitat la ora:", checkboxfont, Brushes.Black, x + 120, y);
                g.DrawString("00:00", checkboxfont, Brushes.Black, x + 270, y);
                y += plus_height;
                g.DrawString("☐36-Stop CR", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐40-Reuşit", checkboxfont, Brushes.Black, x + 120, y);
                y += plus_height;
                g.DrawString("☐37-Cu manevre", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐41-Nereusi oră deces:", checkboxfont, Brushes.Black, x + 120, y);
                g.DrawString("00:00", checkboxfont, Brushes.Black, x + 270, y);
                y += plus_height;
                g.DrawString(" de resuscitare in", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐42-Motivul neînceprii resuscitării:", checkboxfont, Brushes.Black, x + 120, y);
                y += plus_height;
                g.DrawString("curs de desfasurare", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Moarte clinica", checkboxfont, Brushes.Black, x + 120 + g.MeasureString("Moarte clinica", checkboxfont).Width / 2, y);
                y += plus_height;
                g.DrawString("☐38-Trauma", checkboxfont, Brushes.Black, x, y);
            }//functii vitale la preluare
            x = pozx + 605; y = pozy;
            {
                y += plus_height / 5;
                g.DrawString("☐30-Resuscitat", checkboxfont, Brushes.Black, x, y);
                y += plus_height * 6 / 5;
                g.DrawString("☐31-Critic", checkboxfont, Brushes.Black, x, y);
                y += plus_height * 6 / 5;
                g.DrawString("☐32-Urgent", checkboxfont, Brushes.Black, x, y);
                y += plus_height * 6 / 5;
                g.DrawString("☐33-Non-urgent", checkboxfont, Brushes.Black, x, y);
                y += plus_height * 6 / 5;
                g.DrawString("☐34-Consult", checkboxfont, Brushes.Black, x, y);


            }
            x = pozx + 600; y = pozy + plus_height * 6;
            {
                g.DrawString("Ora", checkboxfont, Brushes.Black, x + 25 - g.MeasureString("Ora", checkboxfont).Width / 2, y);
                g.DrawString("00:00", checkboxfont, Brushes.Black, x + 60, y);
                y += plus_height;
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                g.DrawLine(Pens.DarkGray, x + 25, y, x + 25, y + 3 * plus_height);
                g.DrawString("G", checkboxfont, Brushes.Black, x + 12.5f - g.MeasureString("O", checkboxfont).Width / 2, y);
                g.DrawString("M", checkboxfont, Brushes.Black, x + 25 + 12.5f - g.MeasureString("O", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                y += plus_height;
                g.DrawLine(Pens.DarkGray, x + 25, y, width, y);
                g.DrawString("C", checkboxfont, Brushes.Black, x + 12.5f - g.MeasureString("O", checkboxfont).Width / 2, y);
                g.DrawString("V", checkboxfont, Brushes.Black, x + 25 + 12.5f - g.MeasureString("O", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                y += plus_height;
                g.DrawLine(Pens.DarkGray, x + 25, y, width, y);
                g.DrawString("S", checkboxfont, Brushes.Black, x + 12.5f - g.MeasureString("O", checkboxfont).Width / 2, y);
                g.DrawString("O", checkboxfont, Brushes.Black, x + 25 + 12.5f - g.MeasureString("O", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                y += plus_height;
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                g.DrawString("GCS", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("GSC", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                y += plus_height;
                g.DrawString("Fr res", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("Fr res", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                y += plus_height;
                g.DrawString("AV", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("AV", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                y += plus_height;
                g.DrawString("Puls", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("Puls", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                y += plus_height;
                g.DrawString("TA", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("TA", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                y += plus_height;
                g.DrawString("Sat O2", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("Sat 02", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                y += plus_height;
                g.DrawString("Temp", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("Temp", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                y += plus_height;
                g.DrawString("Gli", checkboxfont, Brushes.Black, x + 25f - g.MeasureString("Gli", checkboxfont).Width / 2, y);
                g.DrawString("00", checkboxfont, Brushes.Black, x + 60, y);
                g.DrawLine(Pens.DarkGray, x, y, width, y);
                g.DrawLine(Pens.DarkGray, x + 50, pozy + plus_height * 6, x + 50, y + plus_height);
            }
            x = pozx; y = pozy + plus_height * 18;
            {
                g.DrawImage(global::Server.Properties.Resources.child, x + 275, 20 * plus_height + y);
                g.DrawString("ANAMNEAZA", subtitle, Brushes.Black, x + (275 + 120) / 2 - g.MeasureString("ANAMNEAZA", subtitle).Width / 2, y);
                g.DrawString("TRIAJ", subtitle, Brushes.Black, width - (275 + 120) / 2 + g.MeasureString("TRIAJ", subtitle).Width / 2, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("ANTEGEDENTE PATOLOGICE", subtitle, Brushes.Black, x, y);
                g.DrawString("Observatii", checkboxfont, Brushes.Black, x + 285, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐CARDIACE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐NEUROLOGICE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐RENALE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐PULMONARE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐TBC", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐HEPATICE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐GASTRICE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐DIABET ZAHARAT", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐BOLI INF.-CONTAGIOASE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐BOLI CU TRANSM. SEXUALĂ", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("☐ALTELE", normal, Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("ANAMNEAZA", normal, Brushes.Black, x, y);

                y += g.MeasureString("TRIAJ", subtitle).Height * 24;

                g.DrawString("Alergic la", new Font(normal, FontStyle.Italic), Brushes.Black, x, y);
                y += g.MeasureString("TRIAJ", subtitle).Height;
                y += g.MeasureString("TRIAJ", subtitle).Height;
                g.DrawString("Tratament la domiciliu", new Font(normal, FontStyle.Italic), Brushes.Black, x, y);


            }
            x = pozx + 275 + 130;y= y = pozy + plus_height * 19;
            {
                float constant = 1.225f;
                y += plus_height / 2;
                g.DrawString("General", subtitle, Brushes.Black, x, y);
                g.DrawString("Ochi", subtitle, Brushes.Black, x+175, y);

                y += plus_height * constant;
                g.DrawString("☐14-Talie", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐66-Pierderea ac. a ved.", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐15-Greutate", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐67-Tulb. de vedere", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐16-Febra", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐68-Corp strain intraoc.", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐17-Astenie", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐69-Alte manif. oculare", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐18-Ameteli", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Torace-respirator", subtitle, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("Arsuri", subtitle, Brushes.Black, x, y);
                g.DrawString("☐111-Durere toracica", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐19-Căi resp. s. afectate", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐112-Dispnee", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐20-Flacara", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐113-Hemoptizie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐21-Solid", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐114-Tuse", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐22-Lichid", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐115-Expectoratie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐23-Vapor/gaz", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Gastrointestinal", subtitle, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐24-Chimic", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐132-Greata", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("Psihiatric", subtitle, Brushes.Black, x, y);
                g.DrawString("☐133-Voma", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐246-Depresie", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐134-Tulb. tranzit", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐247-Tulb. comportament", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐135-Rectorargie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐248-Suicid", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐136-Melena", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐225-Halucinatii", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐137-Hematatemeă", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐226-Delir", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐136-Dureri abd.", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("Neurologic", subtitle, Brushes.Black, x, y);
                g.DrawString("Genito urinar", subtitle, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐237-Convulsii", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐176-Tulb. micţiune", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐236-Mioclonii", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐179-Disurie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐249-Cefalee", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐180-Polakurie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐250-Paralizie", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐181-Oligurie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("Tegumente", subtitle, Brushes.Black, x, y);
                g.DrawString("☐177-Hematourie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐141-Calde", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐182-Sâng. vaginală", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐142-Reci", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐183-Sarcina", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐143-Umede", checkboxfont, Brushes.Black, x, y);
                g.DrawString("Ap. locomotor", subtitle, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐151-Palide", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐211-Inflamaţie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐152-Cianotice", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐205-Tunefactie", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐149-Icterice", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐204-Durere", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐146-Echimoze", checkboxfont, Brushes.Black, x, y);
                g.DrawString("☐207-Imp.Funcţ.", checkboxfont, Brushes.Black, x + 170, y);

                y += plus_height * constant;
                g.DrawString("☐154-Eruptii", checkboxfont, Brushes.Black, x, y);
               

                y += plus_height * constant;
                g.DrawString("☐144-Prurit", checkboxfont, Brushes.Black, x, y);
                

                y += plus_height * constant;
                g.DrawString("☐155-Arsuri", checkboxfont, Brushes.Black, x, y);
               


            }
        }
        private void page2(Graphics g,Rectangle bound)
        {
            float x = bound.X / 2, y = bound.Y / 2, width = bound.X * 1.5f + bound.Width, height = bound.Y * 1.5f + bound.Height;
            g.DrawLine(Pens.Black, x, y, width, y);
            g.DrawLine(Pens.Black, x, y, x, height);
            g.DrawLine(Pens.Black, width, y, width, height);
            g.DrawLine(Pens.Black, x,height, width, height);
            g.DrawString(pagenr.ToString(), normal, Brushes.Black, width / 2 - g.MeasureString(pagenr.ToString(), normal).Width / 2, height);
            g.DrawString("Examen Obiectiv", subtitle, Brushes.Black, x, y);
            g.FillRectangle(fill, x, y, width-x, g.MeasureString("E", subtitle).Height);

            float plus_height = g.MeasureString("D", checkboxfont).Height;
            y += g.MeasureString("E", subtitle).Height;
            g.DrawLine(Pens.Black, x, y, width, y);
            g.DrawString("STARE GENERALA", subtitle, Brushes.Black, x, y);
            g.DrawString("☐10-Normală", checkboxfont, Brushes.Black, x+180, y+2.5f);
            g.DrawString("☐11-Influentată", checkboxfont, Brushes.Black, x + 325, y+2.5f);
            g.DrawString("☐12-Alterată", checkboxfont, Brushes.Black, x + 450, y+2.5f);
            g.DrawString("☐13-Profund alterată", checkboxfont, Brushes.Black, x + 550, y+2.5f);

            y += plus_height * 1.1f;
            g.DrawString("Alte:", checkboxfont, Brushes.Black, x , y);
            g.DrawString("not", checkboxfont, Brushes.Black, x + g.MeasureString("Alte:  ", checkboxfont).Width, y);

            y += plus_height;
            g.DrawLine(Pens.Black, x, y, width, y);
            g.DrawLine(Pens.Black, x+362, y, x + 362, y+(1.1f* plus_height*11));
            g.DrawString("CAP", subtitle, Brushes.Black, x, y);
            g.DrawString("GÂT", subtitle, Brushes.Black, x+200, y);
            g.DrawString("NAS", subtitle, Brushes.Black, x+365, y);
            g.DrawString("Stg", checkboxfont, Brushes.Black, x+580, y);
            g.DrawString("Dr", checkboxfont, Brushes.Black, x+700, y);

            y += plus_height*1.1f;
            g.DrawString("☐30-Normal", checkboxfont, Brushes.Black, x, y);
            g.DrawString("☐34-Normal", checkboxfont, Brushes.Black, x + 200, y);
            g.DrawString("Normal", new Font(checkboxfont,FontStyle.Underline), Brushes.Black, x + 365, y);
            g.DrawString("☐40-Epidtaxis", checkboxfont, Brushes.Black, x + 580, y);
            g.DrawString("-41☐", checkboxfont, Brushes.Black, x + 680, y);

            y += plus_height * 1.1f;
            g.DrawString("☐31-Marcă traumatică", checkboxfont, Brushes.Black, x, y);
            g.DrawString("☐35-Marcă traumatică", checkboxfont, Brushes.Black, x + 200, y);
            g.DrawString("☐38-Narile", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x + 365, y);
            g.DrawString("☐42-Corpi straini", checkboxfont, Brushes.Black, x + 580, y);
            g.DrawString("-43☐", checkboxfont, Brushes.Black, x + 680, y);

            y += plus_height * 1.1f;
            g.DrawString("☐32-Leziuni cav. bucală", checkboxfont, Brushes.Black, x, y);
            g.DrawString("☐36-Formaţiuni palpabile", checkboxfont, Brushes.Black, x + 200, y);
            g.DrawString("☐39-Mucoasa nazala", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x + 365, y);
            g.DrawString("☐44-Traumă", checkboxfont, Brushes.Black, x + 580, y);

            y += plus_height * 1.1f;
            g.DrawString("☐33-Leziuni dentare", checkboxfont, Brushes.Black, x, y);
            g.DrawString("☐37-Marcă traumatică", checkboxfont, Brushes.Black, x + 200, y);
            g.DrawString("Alte:", checkboxfont, Brushes.Black, x + 365, y);
            y += plus_height * 1.1f;
            g.DrawLine(Pens.Black, x, y, width, y);
            g.DrawString("APARAT AUDITIV", subtitle, Brushes.Black, x, y);
            g.DrawString("OCHI", subtitle, Brushes.Black, x + 365, y);
            g.DrawString("Stg", checkboxfont, Brushes.Black, x + 550, y);
            g.DrawString("Dr", checkboxfont, Brushes.Black, x + 700, y);
            y += plus_height * 1.1f;
            g.DrawString("Normal", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x, y);
            g.DrawString("Stg", checkboxfont, Brushes.Black, x + 210, y);
            g.DrawString("Dr", checkboxfont, Brushes.Black, x + 340, y);
            g.DrawString("Normal", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x + 365, y);
            g.DrawString("☐56-Conjunctivita", checkboxfont, Brushes.Black, x + 530, y);
            g.DrawString("-57☐", checkboxfont, Brushes.Black, x + 680, y);
            y += plus_height * 1.1f;
            g.DrawString("☐45-Membrana timpanica", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x, y);
            g.DrawString("☐48-Otoragie", checkboxfont, Brushes.Black, x + 210, y);
            g.DrawString("-49☐", checkboxfont, Brushes.Black, x + 320, y);
            g.DrawString("☐54-Mobilitatea globi ocular", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x + 365, y);
            g.DrawString("☐56-Midriază", checkboxfont, Brushes.Black, x + 530, y);
            g.DrawString("-57☐", checkboxfont, Brushes.Black, x + 680, y);
            y += plus_height * 1.1f;
            g.DrawString("☐46-Cai auditive externe", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x, y);
            g.DrawString("☐50-Corpi straini", checkboxfont, Brushes.Black, x + 210, y);
            g.DrawString("-51☐", checkboxfont, Brushes.Black, x + 320, y);
            g.DrawString("☐55-Pupile", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x + 365, y);
            g.DrawString("☐58-Mioza", checkboxfont, Brushes.Black, x + 530, y);
            g.DrawString("-59☐", checkboxfont, Brushes.Black, x + 680, y);
            y += plus_height * 1.1f;
            g.DrawString("☐47-Pavilionul urechii", new Font(checkboxfont, FontStyle.Underline), Brushes.Black, x, y);
            g.DrawString("☐52-Hemotimpan", checkboxfont, Brushes.Black, x + 210, y);
            g.DrawString("-53☐", checkboxfont, Brushes.Black, x + 320, y);
            g.DrawString("Alte:", checkboxfont, Brushes.Black, x + 365, y);
            g.DrawString("☐58-Nistagmus", checkboxfont, Brushes.Black, x + 530, y);
            g.DrawString("-59☐", checkboxfont, Brushes.Black, x + 680, y);
            y += plus_height * 1.1f;
            g.DrawString("Alte:", checkboxfont, Brushes.Black, x, y);
           
            g.DrawString("☐64-Deviatie globi oculari", checkboxfont, Brushes.Black, x + 530, y);
            g.DrawString("-65☐", checkboxfont, Brushes.Black, x + 680, y);
            y += plus_height * 1.1f;
            g.DrawLine(Pens.Black, x, y, width, y);
        }

    }
}
