﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Sockets;
using Newtonsoft.Json;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Server
{
    class Client
    {
        private bool Finish = true;
        private Thread tread;
        private Socket socket;
        private static string connectionString=null;
        public Client()
        {
            Finish = true;
            if (connectionString == null)
            {

             string server = "localhost";
             string  database = "pacientrecord";
             string   uid = "root";
             string   password = "";
             
             connectionString = "datasource=" + server + ";" + "DATABASE=" + database + ";" + "username=" + uid + ";" + "PASSWORD=" + password + ";";


           
                
               
            }
        }
        public bool finish()
        {
            return Finish;
        }
      public void start(Socket sock)
        {
            socket = sock;
            tread = new Thread(new ThreadStart(run));
            tread.Start();


        }
       public void run()
        {
            byte[] buffer = new byte[4096];
            int len; string s=string.Empty;
            socket.ReceiveTimeout = 30;
            do
            {
                try
                {
                    len = socket.Receive(buffer);
                    Thread.Sleep(10);
                }
                catch { break; }
                
                string ss = Encoding.UTF8.GetString(buffer,0,len);
              
                    s += ss;
               
            } while (len==buffer.Length);
           
            MySqlConnection connection = new MySqlConnection(connectionString);
            try
            {
                connection.Open();
                if (s.Length > 0)
                    s = new ProcessInput().process(s, connection);
                

            //    System.Console.Out.Write(s);
                socket.Send(Encoding.UTF8.GetBytes(s));
            }
            catch (Exception e) {
            }
            finally
            {
                connection.Close();
                socket.Close();
            }

        }

 
    }
}
