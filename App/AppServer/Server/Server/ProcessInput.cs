﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Server.tasck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class ProcessInput
    {
        private string UId = string.Empty;
        public string process(string s,MySqlConnection con)
        {
            string result = "";
            dynamic obj=JsonConvert.DeserializeObject(s);
           var c= obj.Count;
            string action = obj["action"];
            string values = obj["values"];
            string Uid = obj["Uid"];
            string fileId = obj["Fid"];
            if(action=="login")
                result=new Login().login(values,con);
            if (action == "get")
                result = new Get().get(values, con,Uid,fileId);
            if(action == "set")
                result = new Set().set(values, con, Uid, fileId);
            if(action == "register")
                result = new Register().register(values, con);
            if (action == "transfer")
                result = new Transfer().transfer(values, con, Uid, fileId);

            response r = new response();
            r.result = result;
            return JsonConvert.SerializeObject(r);
        }
        private struct response{
            public string result { get; set; } 
        }
     
    }
}
