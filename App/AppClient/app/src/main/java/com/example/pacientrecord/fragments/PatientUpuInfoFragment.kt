package com.example.pacientrecord.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pacientrecord.MedicalFilesContent
import com.example.pacientrecord.R
import com.example.pacientrecord.adapters.ItemReadOnlyViewAdapter
import kotlinx.android.synthetic.main.fragment_patient_upu.*

class PatientUPUFragment : Fragment() {

    private fun initRecycleView() {
        patient_upu_details.adapter = ItemReadOnlyViewAdapter(this.requireContext(), MedicalFilesContent.contentUpuSheet())
        patient_upu_details.layoutManager = LinearLayoutManager(this.context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycleView()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_patient_upu, container, false)
    }

    companion object {
        fun newInstance(): PatientUPUFragment = PatientUPUFragment()
    }
}
