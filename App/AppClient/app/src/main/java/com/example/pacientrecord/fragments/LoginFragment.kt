package com.example.pacientrecord.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.pacientrecord.Communication
import com.example.pacientrecord.NetworkSender
import com.example.pacientrecord.R
import com.example.pacientrecord.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.concurrent.thread


class LoginFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    companion object {
        fun newInstance(): LoginFragment =
            LoginFragment()
    }

    @ExperimentalStdlibApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sign_up.setOnClickListener {
            val registerFragment =
                RegisterFragment.newInstance()
            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.login_fragments_container, registerFragment)
            transaction?.addToBackStack("registerFragment")
            transaction?.commit()
        }

        login.setOnClickListener {
            input_errorView.text = ""
            var data=
                URLEncoder.encode("user","UTF-8") + "="+
           URLEncoder.encode( username.text.toString(),"UTF-8") + "&" +
            URLEncoder.encode("pass","utf-8") + "=" +
            URLEncoder.encode( password.text.toString(),"utf-8")

            var address =
                "http://" + Communication.domain + "/login.php"
            var url = URL(address)
            thread {
                try{
                var conn = url.openConnection()
                conn.doOutput=true
                var outputStream=OutputStreamWriter(conn.getOutputStream())
                outputStream.write(data);
                outputStream.flush();
               var scanner= Scanner(conn.getInputStream())
               var line= scanner.nextLine()
                if(line!="")
                {
                    try {
                        var id= line.toLong().toString()
                        Communication.Uid=id;
                        activity?.runOnUiThread {
                            activity?.startActivity(
                                Intent(
                                    activity,
                                    MainActivity::class.java
                                )
                            )
                            activity?.finish()
                        }
                    }catch(e:java.lang.Exception)
                    {
                       activity?.runOnUiThread {
                           input_errorView.text=e.message
                       }
                    }
                }
                   }catch(E:Exception) {
                    activity?.runOnUiThread {
                        input_errorView.text = "Eroare la conexiune"
                    }
                }
            }


        }
    }

}
