package com.example.pacientrecord.localdb.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "filePatient")
data class FilePatient(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    @ColumnInfo(name = "created_date") val createdDate: Date = Date(),
    @ColumnInfo(name = "modified_date") var modifiedDate: Date,
    @ColumnInfo(name = "active") var active: Boolean = true,
    @ColumnInfo(name = "userId") var userId: Long = 0

)