package com.example.pacientrecord.activities

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.annotation.IntegerRes
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pacientrecord.Communication
import com.example.pacientrecord.MedicalFilesContent
import com.example.pacientrecord.R
import com.example.pacientrecord.adapters.ItemViewAdapter
import com.example.pacientrecord.data.*
import com.example.pacientrecord.localdb.models.FileDetail
import com.example.pacientrecord.localdb.models.FilePatient
import com.example.pacientrecord.localdb.models.FilePatientDetail
import com.example.pacientrecord.localdb.repos.FileDetailRepo
import com.example.pacientrecord.localdb.repos.FilePatientDetailRepo
import com.example.pacientrecord.localdb.repos.FilePatientRepo
import com.example.pacientrecord.popups.ShowQRCode
import kotlinx.android.synthetic.main.activity_newpacient.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.OutputStreamWriter
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

class NewPatientActivity() : AppCompatActivity() {
     private var selectedMedicalFileId : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED
        setContentView(R.layout.activity_newpacient)
        selectedMedicalFileId = intent.getIntExtra("selectedId", 0)
init()
    }

    @SuppressLint("RestrictedApi", "ResourceType")
   fun init() {
       floatingActionMore.setOnClickListener {
           if (floatingActionButtonSend.visibility == View.VISIBLE) {
               floatingActionButtonSend.visibility = View.GONE
               floatingActionButtonDelete.visibility = View.GONE
               floatingActionButtonSave.visibility = View.GONE
               floatingActionMore.foreground = Drawable.createFromXml(
                   resources,
                   resources.getXml(R.drawable.ic_add_circle_outline_black_24dp)
               )
           } else {
               floatingActionButtonSend.visibility = View.VISIBLE
               floatingActionButtonDelete.visibility = View.VISIBLE
               floatingActionButtonSave.visibility = View.VISIBLE
               floatingActionMore.foreground = Drawable.createFromXml(
                   resources,
                   resources.getXml(R.drawable.ic_remove_circle_outline_black_24dp)
               )

           }
       }

        floatingActionMore.alpha=0.5f

        var data : ArrayList<Data>

        when (selectedMedicalFileId) {
            R.id.menu_item_upu_sheet -> {
                data = MedicalFilesContent.contentUpuSheet()
            }

            R.id.menu_item_exit_sheet -> {
                data = MedicalFilesContent.contentExitSheet()
            }

            else -> data = MedicalFilesContent.contentUpuSheet()
        }
        content.adapter = ItemViewAdapter(
            this,
             data
        )

        if(Communication.fid!="")
        {
            thread {
               var data: ArrayList<Data> = ArrayList<Data>()
                var id:Long?
                if(Communication.fid.contains("L")) {
                     id =
                        Communication.fid.substring(
                            0,
                            Communication.fid.length - 1
                        ).toLong()

                    data =loadDataFromLocalDb(id);
                }
                else {

                    var address =
                        "http://" + Communication.domain + "/get.php?User=" + URLEncoder.encode(
                            Communication.GetUid(), "UTF-8"
                        ) +
                                "&File=" + URLEncoder.encode(Communication.fid, "UTF-8")
                    var url = URL(address)
                    var conn = url.openConnection()
                    var scanner = Scanner(conn.getInputStream())

                    while (scanner.hasNext()) {
                        try {

                                var obj = JSONObject(scanner.nextLine())
                                var content = obj.getString("content")
                                var hint = obj.getString("hint")
                                var ischecked = obj.getInt("ischecked")
                                var left = obj.getInt("left")
                                var name = obj.getString("name")
                                var right = obj.getInt("right")
                                var type = obj.getInt("type")
                                var size = obj.getDouble("size")

                            if(type==0)
                            {
                                var info=Data();
                                info.content=content
                                info.name=name
                                info.type=type
                                info.textsize= size.toFloat()
                                data.add(info)
                            }
                            if(type==1)
                            {
                                var info=EditText();
                                info.content=content
                                info.name=name
                                info.type=type
                               // info.textsize= size.toFloat()
                                info.hint=hint
                                data.add(info)
                            }
                            if(type==2)
                            {
                                var info=CheckBox();
                                info.content=content
                                info.name=name
                                info.type=type
                              //  info.textsize= size.toFloat()
                                info.isChecked=(ischecked==1)
                                data.add(info)
                            }
                            if(type==4)
                            {
                                var info=Dual();
                                info.content=content
                                info.name=name
                                info.type=type
                                //info.textsize= size.toFloat()
                                info.left=(left==1)
                                info.right=(right==1)
                                data.add(info)
                            }


                        } catch (e: Exception) {
e.stackTrace
                        }
                    }
                }
                if(data.size!=0)
                    runOnUiThread{
                        (content.adapter as ItemViewAdapter).update(data)
                    }

            }
        }
       content.layoutManager= LinearLayoutManager(this)

        floatingActionButtonSave.setOnClickListener {
            thread {
                if(Communication.fid=="")
                saveFileLocally(null, data)
                else {
                    val id : Long

                    if(Communication.fid.contains("L")) {
                         id =
                            Communication.fid.substring(
                                0,
                                Communication.fid.length - 1
                            ).toLong()
                    }
                    else id = Communication.fid.toLong()

                    saveFileLocally(id, data)
                }
            }
        }

        floatingActionButtonDelete.setOnClickListener {

        }

        floatingActionButtonSend.setOnClickListener {
             floatingActionButtonSend.requestFocus()
              ShowQRCode()
                  .showPopupWindow(content,this)
        }

   }



    private fun loadDataFromLocalDb(id : Long) : ArrayList<Data> {
        var fileDetails = FilePatientDetailRepo().getAllDetailsForFile(id)
        var data = ArrayList<Data>()

        for(detail in fileDetails) {
            when(detail.type) {
                0 -> {
                    var element: Data = Data()
                    element.content = detail.content
                    element.textsize = detail.txtSize
                    element.type = detail.type
                    data.add(element)
                }
                1 -> {
                    var element: Data = EditText()
                    element.content = detail.content
                    element.type = detail.type
                    element.name = detail.name!!
                    data.add(element)
                }
                2 -> {
                    var element: CheckBox = CheckBox()
                    element.content = detail.content
                    element.type = detail.type
                    element.name = detail.name!!
                    element.isChecked = detail.isChecked!!
                    data.add(element)
                }
                4 -> {
                    var element: Dual = Dual()
                    element.content = detail.content
                    element.type = detail.type
                    element.name = detail.name!!
                    element.left = detail.left!!
                    element.right = detail.right!!
                    data.add(element)
                }

                else -> {}
            }
        }
        return data
    }

    private fun saveFileLocally(fileId: Long?, data : ArrayList<Data>) {
        var fileDetailRepo = FileDetailRepo()
        var filePatientRepo = FilePatientRepo()
        var filePatientFileDetailRepo = FilePatientDetailRepo()

        var detailId : Long? = null
        lateinit var detail : FileDetail
        lateinit var filePatientDetail: FilePatientDetail
        var fId : Long = 0

        fId = if(fileId==null) {
            var filePatient =
                FilePatient(modifiedDate = Date(), userId = Communication.Uid.toLong())
            filePatientRepo.insert(filePatient)
        } else {
            fileId
        }

        for((order, info : Data) in data.withIndex()) {

            when(info.type) {
                0 -> {
                    detailId = fileDetailRepo.findId(
                        content = info.content,
                        type = info.type,
                        txtSize = info.textsize
                    )

                    if (detailId == null) {
                        detail = FileDetail(content = info.content, type = info.type, txtSize = info.textsize)
                        detailId = fileDetailRepo.insert(detail)
                    }
                }
                1 -> {
                    detailId = fileDetailRepo.findId(
                        content = info.content,
                        type = info.type,
                        txtSize = info.textsize,
                        name = info.name
                    )
                    if (detailId == null) {
                        detail =
                            FileDetail(content = info.content, type = info.type, name = info.name, txtSize = info.textsize)
                        detailId = fileDetailRepo.insert(detail)
                    }
                }
                2 -> {
                    detailId = fileDetailRepo.findId(
                        content = info.content,
                        type = info.type,
                        txtSize = info.textsize,
                        name = info.name,
                        isChecked = (info as CheckBox).isChecked
                    )

                    if (detailId == null) {
                        detail = FileDetail(
                            content = info.content,
                            type = info.type,
                            txtSize = info.textsize,
                            name = info.name,
                            isChecked = info.isChecked
                        )
                        detailId = fileDetailRepo.insert(detail)
                    }
                }
                4 -> {
                    detailId = fileDetailRepo.findId(
                        content = info.content,
                        type = info.type,
                        txtSize = info.textsize,
                        name = info.name,
                        left = (info as Dual).left,
                        right = (info as Dual).right
                    )

                    if (detailId == null) {
                        detail = FileDetail(
                            content = info.content,
                            type = info.type,
                            txtSize = info.textsize,
                            name = info.name,
                            left = (info as Dual).left,
                            right = (info as Dual).right
                        )
                        detailId = fileDetailRepo.insert(detail)
                    }
                }

                else -> {}
            }

            if(fileId==null || filePatientFileDetailRepo.find(fId, order) == null) {
                filePatientDetail =
                    FilePatientDetail(detailId = detailId!!, fileId = fId, order = order)
                filePatientFileDetailRepo.insert(filePatientDetail)
            }
            else {
                filePatientDetail = filePatientFileDetailRepo.find(fileId, order)!!
                filePatientDetail.detailId = detailId!!
                filePatientFileDetailRepo.update(filePatientDetail)
            }
        }
    }

    private fun getUserCnp() : String? {
        var data = (content.adapter as ItemViewAdapter).getContent()
        var detail = data.find { detail -> detail.content == "Pacient-CNP" }

        return detail?.content
    }

    private fun sendFileToUser(cnp : String) : Boolean {

        var uid: Long? = Communication.getUid(cnp) ?: return false

        var address = "http://" + Communication.domain + "/set.php?User=" + URLEncoder.encode(
            uid!!.toString(), "UTF-8") + "&File=" + URLEncoder.encode(Communication.fid, "UTF-8")

        var inputstream:BufferedReader?=null
        try {
            var url = URL(address)

            var postcontent = URLEncoder.encode(
                "data",
                "UTF-8"
            ) + "=" + URLEncoder.encode((content.adapter as ItemViewAdapter).save(), "UTF-8")

            var conn = url.openConnection()

            conn.doOutput = true

            var outstream = OutputStreamWriter(conn.getOutputStream())
            outstream.write(postcontent)
            outstream.flush()
            var scanner=  Scanner(conn.getInputStream())
            scanner.nextInt().toString();
        }
        catch(E:Exception){
            E.toString()
            return false
        }
        finally {
            try {
                inputstream ?.close()
            } catch (ex: Exception) {
            }
        }
        return true
    }

    fun save()
    {
        var address = "http://" + Communication.domain + "/set.php?User=" + URLEncoder.encode(
            Communication.GetUid(), "UTF-8"
        ) +
                "&File=" + URLEncoder.encode(Communication.fid, "UTF-8")

        var inputstream:BufferedReader?=null
        var text:String=""
        try {
            var url = URL(address)

            var postcontent = URLEncoder.encode(
                "data",
                "UTF-8"
            ) + "=" + URLEncoder.encode((content.adapter as ItemViewAdapter).save(), "UTF-8")

            var conn = url.openConnection()

            conn.doOutput = true

            var outstream = OutputStreamWriter(conn.getOutputStream())
            outstream.write(postcontent)
            outstream.flush()
            var scanner=  Scanner(conn.getInputStream())
            text=scanner.nextInt().toString();
        }
        catch(E:Exception){
            E.toString()
        }
        finally {
            try {
                inputstream ?.close()
                Communication.fid = text

            } catch (ex: Exception) {
            }
        }

        /*var cnp = getUserCnp()
        if(cnp != null) {
            sendFileToUser(cnp)
        }*/

    }

}



