package com.example.pacientrecord.adapters

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.pacientrecord.R
import com.example.pacientrecord.data.Data

class ItemReadOnlyViewAdapter(context: Context, var data: ArrayList<Data>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        if (viewType == 0) {
            return TextHolder(
                inflater.inflate(
                    R.layout.item_text_view,
                    parent,
                    false
                )
            )
        }
        if (viewType == 4) {
            return DualCheckboxHolder(
                inflater.inflate(
                    R.layout.item_dual_check,
                    parent,
                    false
                )
            )
        }

        if (viewType == 1) {
            return EditTextHolder(
                inflater.inflate(
                    R.layout.item_edit_text,
                    parent,
                    false
                )
            )
        }
        if (viewType == 2) {
            return checkBoxHolder(
                inflater.inflate(
                    R.layout.item_check_box,
                    parent,
                    false
                )
            )
        }
        if (viewType == 3) {
            return RadioButonHolder(
                inflater.inflate(
                    R.layout.item_radio_button,
                    parent,
                    false
                )
            )
        }

        return TextHolder(
            inflater.inflate(
                R.layout.item_text_view,
                parent,
                false
            )
        )
    }

    override fun getItemViewType(position: Int): Int {

        return data[position].type

    }

    override fun getItemCount(): Int {
        return data.size
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var currentData = data[position]
        val type = currentData.type
        if (type == 0) {
            (holder as TextHolder).text.text = data[position].content
            holder.text.setBackgroundColor(Color.parseColor(currentData.bgColor))
            holder.text.setTextColor(Color.parseColor(currentData.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            //readonly
            return
        }

        if (type == 1) {
            (holder as EditTextHolder).text.setText(currentData.content)
            holder.id = position
            holder.text.setBackgroundColor(Color.parseColor(currentData.bgColor))
            holder.text.setTextColor(Color.parseColor(currentData.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            holder.text.isEnabled = false

            return
        }

        if (type == 2) {
            (holder as checkBoxHolder).text.text = currentData.content
            holder.text.setBackgroundColor(Color.parseColor(currentData.bgColor))
            holder.text.setTextColor(Color.parseColor(currentData.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            holder.text.isChecked =
                (currentData as com.example.pacientrecord.data.CheckBox).isChecked
            holder.text.isEnabled = false
            return
        }

        if (type == 3) {
            (holder as RadioButonHolder).text.text = currentData.content
            holder.text.setBackgroundColor(Color.parseColor(currentData.bgColor))
            holder.text.setTextColor(Color.parseColor(currentData.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            holder.text.isEnabled = false

            return
        }
        if (type == 4) {
            (holder as DualCheckboxHolder).text.text = currentData.content
            holder.text.setBackgroundColor(Color.parseColor(currentData.bgColor))
            holder.text.setTextColor(Color.parseColor(currentData.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            holder.left.isChecked = (currentData as com.example.pacientrecord.data.Dual).left
            holder.right.isChecked = currentData.right
            holder.left.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            holder.right.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
            holder.left.isEnabled = false
            holder.right.isEnabled = false
            var width = holder.right.width
            return
        }

        (holder as TextHolder).text.text = currentData.content
        holder.text.setBackgroundColor(Color.parseColor(currentData.bgColor))
        holder.text.setTextColor(Color.parseColor(currentData.textColor))
        holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, currentData.textsize)
        holder.text.isEnabled = false
    }

}


