package com.example.pacientrecord

import android.Manifest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.activity_q_r_scanner.*
import java.io.OutputStreamWriter
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.concurrent.thread

class QRScanner : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_q_r_scanner)
        Dexter.withActivity(this).withPermission(Manifest.permission.CAMERA)
            .withListener(object:PermissionListener{
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    scanner.setResultHandler {
                        thread {
                            var addres =
                                "http://" + Communication.domain + "/transferfrom.php?User=" + URLEncoder.encode(
                                    Communication.GetUid(),
                                    "UTF-8"
                                ) +
                                        "&file=" + URLEncoder.encode(Communication.fid, "UTF-8")

                            var inputstream:Scanner?=null
                            var text: String = ""
                            try {
                                var url = URL(addres)

                                var postcontent = URLEncoder.encode(
                                    "data",
                                    "UTF-8"
                                ) + "=" + URLEncoder.encode(it.text, "UTF-8")

                                var conn = url.openConnection()
                                conn.doOutput = true
                                var outstream = OutputStreamWriter(conn.getOutputStream())
                                outstream.write(postcontent)
                                outstream.flush()
                                inputstream = Scanner(conn.getInputStream())
                                var sb = StringBuilder()
                                var line: String?
                                while (true) {
                                    line = inputstream.nextLine()
                                    if (line == null)
                                        break
                                    sb.append(line + "\n")
                                }

                            } catch (E: Exception) {

                            }

                            runOnUiThread { finish() }
                        }

                    }
                    scanner.startCamera()
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {

                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {

                }
            }).check()
    }

}
