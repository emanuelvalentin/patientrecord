package com.example.pacientrecord.localdb.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.pacientrecord.localdb.models.FileDetail
import com.example.pacientrecord.localdb.models.FilePatientDetail

@Dao
interface FilePatientDetailDao {

    @Insert
    fun insert(filePatientDetail: FilePatientDetail) : Long

    @Query("SELECT * FROM fileDetail INNER JOIN filePatientDetail ON fileDetail.id=filePatientDetail.detailId WHERE filePatientDetail.fileId=:fileId ORDER BY `order`")
    fun selectAllDetailsForFile(fileId : Long) : List<FileDetail>

    @Query("SELECT * FROM filePatientDetail WHERE fileId=:fileId AND `order`=:order")
    fun find(fileId: Long, order: Int) : FilePatientDetail?

    @Update
    fun update(filePatientDetail: FilePatientDetail)
}