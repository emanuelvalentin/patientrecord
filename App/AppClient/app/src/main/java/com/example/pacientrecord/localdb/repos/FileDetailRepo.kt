package com.example.pacientrecord.localdb.repos

import android.annotation.SuppressLint
import android.os.AsyncTask
import com.example.pacientrecord.localdb.AppDatabase
import com.example.pacientrecord.localdb.ApplicationController
import com.example.pacientrecord.localdb.models.FileDetail
import com.example.pacientrecord.localdb.models.FilePatient
import kotlin.concurrent.thread

class FileDetailRepo() {
    private val appDatabase: AppDatabase = ApplicationController.appDatabase!!

    fun findId(
        content : String,
        type : Int,
        txtSize : Float,
        name : String? = null,
        isChecked : Boolean? = null,
        left : Boolean? = null,
        right : Boolean? = null,
        hint : String? = null
    ) : Long? {
        var id = FindIdTask().execute(
            content as Object,
            type as Object,
            txtSize as Object,
            name as Object?,
            isChecked as Object?,
            left as Object?,
            right as Object?,
            hint as Object?
        ).get()
       /* thread{
            try {
                id = appDatabase.fileDetailDao()
                    .findId(content, type, txtSize, name, isChecked, left, right, hint)
            }
            catch(e : Exception) {}
        }*/
        return id;
    }

    fun insert(fileDetail: FileDetail) : Long {
        var id = InsertTask().execute(fileDetail).get()
        /*thread {
            try {
                id = appDatabase.fileDetailDao()
                    .insert(fileDetail)
            }
            catch(e : Exception) {}
        }*/
        return id
    }


    @SuppressLint("StaticFieldLeak")
    private inner class InsertTask() :
        AsyncTask<FileDetail, Void, Long>() {

        override fun doInBackground(vararg params: FileDetail): Long {
            return appDatabase.fileDetailDao().insert(params[0])
        }

    }

    @SuppressLint("StaticFieldLeak")
    private inner class FindIdTask() :
        AsyncTask<Object?, Void, Long?>() {

        override fun doInBackground(vararg params: Object?): Long? {
           return appDatabase.fileDetailDao()
                .findId(
                    params[0]!! as String,
                    params[1]!! as Int,
                    params[2]!! as Float,
                    params[3] as String?,
                    params[4] as Boolean?,
                    params[5] as Boolean?,
                    params[6] as Boolean?,
                    params[7] as String?
                )
        }

    }

}
