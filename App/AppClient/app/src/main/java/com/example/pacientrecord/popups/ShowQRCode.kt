package com.example.pacientrecord.popups


import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.example.pacientrecord.Communication
import com.example.pacientrecord.R
import com.example.pacientrecord.activities.NewPatientActivity
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.concurrent.thread

class ShowQRCode {
    //PopupWindow display method
    fun showPopupWindow(view: View,activity: Activity) {


        //Create a View object yourself through inflater
        val inflater = view.context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.show_q_r_code, null)

        //Specify the length and width through constants
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        //Make Inactive Items Outside Of PopupWindow
        val focusable = true


        //Create a window with our parameters
        val popupWindow = PopupWindow(popupView, width, height, focusable)

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0)

        //Initialize the elements of our window, install the handler
        val imageView = popupView.findViewById<ImageView>(R.id.ImageQRCode)
        val progressBar2=popupView.findViewById<ProgressBar>(R.id.progressBar2)
        progressBar2.visibility=View.VISIBLE
        val mesaje=popupView.findViewById<TextView>(R.id.Errormesaje)
        val buttonEdit =
            popupView.findViewById<Button>(R.id.BtnCancel)
       thread{
           var win = true
           popupWindow.setOnDismissListener { win=false }
           try {
               (activity as NewPatientActivity).save()
               var Urlstring =
                   "http://" + Communication.domain + "/transferimage.php?User=" + URLEncoder.encode(
                       Communication.GetUid(),
                       "utf-8"
                   ) + "&File=" + URLEncoder.encode(Communication.fid, "utf-8")

               var Url = URL(Urlstring)
               var connection = Url.openConnection()
               // connection.
                    var image=BitmapFactory.decodeStream(connection.inputStream)

                   //     var sender = networkSender().send(obj)
                   activity.runOnUiThread {




                       imageView.setImageBitmap(image)
                       progressBar2.visibility=View.GONE
                   }
                
           }
               catch (e:Exception)
               {
                   activity.runOnUiThread {
                       mesaje.visibility = View.VISIBLE
                       mesaje.setOnClickListener {
                           popupWindow.dismiss()
                           showPopupWindow(view, activity)
                       }
                       progressBar2.visibility=View.GONE
                   }
               }
           while(win)
           {
               try {
                   var Url =
                       URL("http://" + Communication.domain + "/istransfered.php?User="+URLEncoder.encode(
                           Communication.GetUid(),"utf-8")+"&File="+URLEncoder.encode(
                           Communication.fid,"utf-8"))
                   var con = Url.openConnection()
                   var sc=Scanner(con.getInputStream())
                  var next= sc.next()
                  if( next.compareTo("Done!")==0)
                      activity.runOnUiThread {
                          imageView.visibility = View.GONE
                          mesaje.setTextColor(Color.parseColor("#00ff00"))
                          mesaje.text="Fisa a fost transferata"
                          mesaje.visibility = View.VISIBLE
                          buttonEdit.setText("Ok");
                          buttonEdit.setOnClickListener {
                              popupWindow.dismiss()
                              activity.runOnUiThread { activity.finish() }
                          }
                          mesaje.setOnClickListener {
                              popupWindow.dismiss()
                              activity.runOnUiThread { activity.finish() }
                          }
                        //  popupWindow.dismiss()
                          //activity.runOnUiThread { activity.finish() }
                      }
                   if(next.compareTo("Expired!")==0)
                   {
                       activity.runOnUiThread {
                           imageView.visibility = View.GONE
                           mesaje.visibility = View.VISIBLE

                           mesaje.setOnClickListener {
                               popupWindow.dismiss()
                               showPopupWindow(view, activity)
                           }
                       }
                   }
               }
               catch (e:Exception){}
               Thread.sleep(10000)
           }
        }



        buttonEdit.setOnClickListener { //As an example, display the message
            popupWindow.dismiss()
        }


        //Handler for clicking on the inactive zone of the window

    }


}