package com.example.pacientrecord.localdb.repos

import android.annotation.SuppressLint
import android.os.AsyncTask
import com.example.pacientrecord.localdb.AppDatabase
import com.example.pacientrecord.localdb.ApplicationController
import com.example.pacientrecord.localdb.models.FileDetail
import com.example.pacientrecord.localdb.models.FilePatientDetail

class FilePatientDetailRepo {
    private val appDatabase: AppDatabase = ApplicationController.appDatabase!!

    fun insert(filePatientDetail: FilePatientDetail): Long {
        return InsertTask().execute(filePatientDetail).get()
    }

    fun getAllDetailsForFile(fileId: Long): List<FileDetail> {
        return GetAllDetailsForFileTask().execute(fileId).get()
    }

    fun find(fileId: Long, order: Int) : FilePatientDetail? {
        return FindTask().execute(fileId as Object, order as Object).get()
    }

    fun update(filePatientDetail: FilePatientDetail) {
        UpdateTask().execute(filePatientDetail)
    }

    @SuppressLint("StaticFieldLeak")
    private inner class InsertTask() :
        AsyncTask<FilePatientDetail, Void, Long>() {

        override fun doInBackground(vararg params: FilePatientDetail): Long {
            return appDatabase.filePatientDetailDao().insert(params[0])
        }

    }

    @SuppressLint("StaticFieldLeak")
    private inner class GetAllDetailsForFileTask() :
        AsyncTask<Long, Void, List<FileDetail>>() {

        override fun doInBackground(vararg params: Long?): List<FileDetail> {
            return appDatabase.filePatientDetailDao()
                .selectAllDetailsForFile(params[0]!!)
        }

    }

    @SuppressLint("StaticFieldLeak")
    private inner class FindTask() :
        AsyncTask<Object, Void, FilePatientDetail?>() {

        override fun doInBackground(vararg params: Object): FilePatientDetail? {
            return appDatabase.filePatientDetailDao().find(params[0] as Long, params[1] as Int)
        }

    }

    @SuppressLint("StaticFieldLeak")
    private inner class UpdateTask() :
        AsyncTask<FilePatientDetail, Void, Void?>() {

        override fun doInBackground(vararg params: FilePatientDetail): Void? {
            appDatabase.filePatientDetailDao().update(params[0])
            return null
        }

    }

}