package com.example.pacientrecord.localdb.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pacientrecord.localdb.models.FileDetail

@Dao
interface FileDetailDao {

    @Insert
    fun insert(fileDetail : FileDetail) : Long

    @Query("Select id from fileDetail where " +
            "content=:content and type=:type and txtSize=:txtSize and EXISTS(Select name INTERSECT Select :name)" +
            "and EXISTS(Select isChecked INTERSECT Select :isChecked) and EXISTS(Select `left` INTERSECT Select :left)" +
            "and EXISTS(Select `right` INTERSECT Select :right) and EXISTS(Select hint INTERSECT Select :hint)")
    fun findId(
        content : String,
        type : Int,
        txtSize : Float,
        name : String? = null,
        isChecked : Boolean? = null,
        left : Boolean? = null,
        right : Boolean? = null,
        hint : String? = null
    ) : Long?

}