package com.example.pacientrecord.localdb.models

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "filePatientDetail")
data class FilePatientDetail (
    @PrimaryKey(autoGenerate = true)
    var id : Long = 0,
    var fileId : Long = 0,
    var detailId : Long = 0,
    var order : Int
)
