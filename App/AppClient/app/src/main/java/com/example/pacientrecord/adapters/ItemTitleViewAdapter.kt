package com.example.pacientrecord.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.pacientrecord.Communication
import com.example.pacientrecord.R
import com.example.pacientrecord.activities.NewPatientActivity
import com.example.pacientrecord.data.DataTitle

class ItemTitleViewAdapter(context: Context, var data: ArrayList<DataTitle>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class TitleViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var title: TextView = item.findViewById(R.id.item_title)
        var subtitle: TextView = item.findViewById(R.id.item_subtitle)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        var view = inflater.inflate(R.layout.item_title_view, parent, false)

        return TitleViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }
    public fun update(data: ArrayList<DataTitle>)
    {

        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()

    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var titleHolder = holder as TitleViewHolder
        titleHolder.title.text = data[position].title
        titleHolder.subtitle.text = data[position].subtitle
        titleHolder.itemView
            .setOnClickListener {

                Communication.fid = data[position].id.toString()
                if( data[position].subtitle.contains("locala"))
                    Communication.fid += "L"
                titleHolder.itemView.context.
                startActivity(Intent( titleHolder.itemView.context, NewPatientActivity::class.java))

        }
    }

}