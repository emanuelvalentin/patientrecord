package com.example.pacientrecord.data

import android.graphics.Color
import org.json.JSONObject

open class Data():java.io.Serializable
{
   public var textsize:Float=20f
    public var content:String=""
    public var textColor:String="#000000";
    public var type:Int=0;
    public var bgColor:String="#ffffff";
    public var name:String=""

    override fun toString(): String {
       return JSONObject().put("content",content)
           .put("name",name).put("type",type).put("size",textsize).toString()
    }
}