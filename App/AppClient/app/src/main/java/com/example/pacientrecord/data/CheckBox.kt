package com.example.pacientrecord.data

import org.json.JSONObject

class CheckBox :Data()
{
    public var isChecked=false;
    override fun toString(): String {
        return JSONObject().put("content",content).put("name",name)
            .put("type",type).put("ischecked",isChecked).toString()
    }
}