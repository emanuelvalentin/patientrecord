package com.example.pacientrecord.localdb.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="fileDetail")
data class FileDetail (
    @PrimaryKey(autoGenerate = true) var id : Long = 0,
    @ColumnInfo(name = "name") val name : String? = null,
    @ColumnInfo(name = "type") val type : Int,
    @ColumnInfo(name = "content") val content : String,
    @ColumnInfo(name = "txtSize") var txtSize : Float,
    @ColumnInfo(name = "isChecked") val isChecked : Boolean? = null,
    @ColumnInfo(name = "hint") var hint : String? = null,
    @ColumnInfo(name = "left") var left : Boolean? = null,
    @ColumnInfo(name = "right") var right : Boolean? = null
    )