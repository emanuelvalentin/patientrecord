package com.example.pacientrecord.localdb.repos

import android.annotation.SuppressLint
import android.os.AsyncTask
import com.example.pacientrecord.localdb.AppDatabase
import com.example.pacientrecord.localdb.ApplicationController
import com.example.pacientrecord.localdb.models.FilePatient

class FilePatientRepo {

    private val appDatabase: AppDatabase = ApplicationController.appDatabase!!

    fun insert(filePatient: FilePatient) : Long {
        return InsertTask().execute(filePatient).get()
    }

    fun getAllIds(userId : Long) : List<Long> {
        return GetAllIdsTask().execute(userId).get()
    }

    @SuppressLint("StaticFieldLeak")
    private inner class GetAllIdsTask() :
        AsyncTask<Long, Void, List<Long>>() {

        override fun doInBackground(vararg params: Long?): List<Long> {
            return appDatabase.filePatientDao().selectAllIdsForUser(params[0]!!)
        }

    }

    @SuppressLint("StaticFieldLeak")
    private inner class InsertTask() :
        AsyncTask<FilePatient, Void, Long>() {

        override fun doInBackground(vararg params: FilePatient): Long {
            return appDatabase.filePatientDao().insert(params[0])
        }

    }



}