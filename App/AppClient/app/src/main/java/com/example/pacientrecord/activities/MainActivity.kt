package com.example.pacientrecord.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.ContextMenu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pacientrecord.Communication
import com.example.pacientrecord.QRScanner
import com.example.pacientrecord.R
import com.example.pacientrecord.adapters.ItemTitleViewAdapter
import com.example.pacientrecord.data.DataTitle
import com.example.pacientrecord.localdb.models.FileDetail
import com.example.pacientrecord.localdb.repos.FilePatientDetailRepo
import com.example.pacientrecord.localdb.repos.FilePatientRepo
import kotlinx.android.synthetic.main.activity_main.*

import org.json.JSONObject
import java.lang.Exception
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    @SuppressLint("RestrictedApi", "ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        registerForContextMenu(fabNew)

if(!Communication.isLogin()) {
    finish()
    startActivity(
        Intent(
            this,
            LoginActivity::class.java
        )
    )

}



        fabMore.setOnClickListener {
                if (fabRecive.visibility == View.VISIBLE) {
                    fabNew.visibility = View.GONE
                    fabRecive.visibility = View.GONE
                    fabRefresh.visibility = View.GONE
                    fabMore.foreground = Drawable.createFromXml(
                        resources,
                        resources.getXml(R.drawable.ic_add_circle_outline_black_24dp)
                    )
                } else {
                    fabNew.visibility = View.VISIBLE
                    fabRecive.visibility = View.VISIBLE
                    fabRefresh.visibility = View.VISIBLE
                    fabMore.foreground = Drawable.createFromXml(
                        resources,
                        resources.getXml(R.drawable.ic_remove_circle_outline_black_24dp)
                    )

                }

        }

        fabNew.setOnClickListener {
            Communication.fid = ""
            startActivity(Intent(this, NewPatientActivity::class.java))
        }
        fabRecive.setOnClickListener {

            startActivity(Intent(this, QRScanner::class.java))

    }
        recycle_titles.adapter = ItemTitleViewAdapter(this, ArrayList<DataTitle>())
        recycle_titles.layoutManager = LinearLayoutManager(this)

        fabRefresh.setOnClickListener {
            updateRecycle()
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        Communication.fid = ""
        startActivity(Intent(this, NewPatientActivity::class.java).putExtra("selectedId", item.itemId))
        return true
    }
    @SuppressLint("RestrictedApi", "ResourceType")
    private fun updateRecycle() {

        thread {
            try {
                var layout = (recycle_titles.layoutManager as LinearLayoutManager)
                var scrollpoz = layout.findFirstCompletelyVisibleItemPosition()
                var adapter = recycle_titles.adapter as ItemTitleViewAdapter

                Communication.fid = "";
                var data = getAllFilesTitles(Communication.Uid.toLong())
                try {
                    var address =
                        "http://" + Communication.domain + "/get.php?User=" + URLEncoder.encode(
                            Communication.GetUid(), "UTF-8"
                        ) +
                                "&File=" + URLEncoder.encode(Communication.fid, "UTF-8")
                    var url = URL(address)
                    var conn = url.openConnection()
                    var scanner = Scanner(conn.getInputStream())

                    while (scanner.hasNext()) {
                        try {
                            var obj = JSONObject(scanner.nextLine())
                            var title = obj.getString("title")
                            var subtitle = obj.getString("subtitle")
                            var id = obj.getLong("id")
                            data.add(DataTitle(title = title, subtitle = subtitle, id = id))
                        } catch (e: Exception) {

                        }
                    }
                } catch(E:Exception){}
                runOnUiThread {
                    adapter.update(data)
                    layout.scrollToPosition(scrollpoz)
                }
            } catch (e: Exception) {

            }
        }
        thread{
            try {
                var address =
                    "http://" + Communication.domain + "/check.php?User=" + URLEncoder.encode(
                        Communication.GetUid(), "UTF-8"
                    )
                var url = URL(address)
                var conn = url.openConnection()
                var scanner = Scanner(conn.getInputStream())
                var scan = scanner.nextLine()
                if (scan.compareTo("medic", true) == 0) {
                    runOnUiThread {
                        fabMore.visibility = View.VISIBLE;
                    }

                } else
                    runOnUiThread {
                        fabMore.visibility = View.GONE;
                        fabNew.visibility = View.GONE
                        fabRefresh.visibility = View.GONE
                        fabRecive.visibility = View.GONE
                        fabMore.foreground = Drawable.createFromXml(
                            resources,
                            resources.getXml(R.drawable.ic_add_circle_outline_black_24dp)
                        )
                    }
            }catch  (e:Exception){}
        }
    }
    private fun getAllFilesTitles(userId : Long) : ArrayList<DataTitle> {
        var ids = FilePatientRepo().getAllIds(userId)
        var title: String
        var subtitle : String = "Fisa UPU locala"
        var filePatientDetailRepo = FilePatientDetailRepo()
        lateinit var fileDetails : List<FileDetail>
        lateinit var info : DataTitle
        var data = ArrayList<DataTitle>()

        for(id : Long in ids) {
            fileDetails = filePatientDetailRepo.getAllDetailsForFile(id)

            var name = fileDetails.find { detail -> detail.name == "Pacient-Nume" }

            if(name != null) {
                title = name.content
            }
            else title = ""

            info = DataTitle(title, subtitle, id)
            data.add(info)
        }
        return data
    }
    override fun onResume() {
        super.onResume()
        updateRecycle()
    }
}
