package com.example.pacientrecord.localdb

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.pacientrecord.localdb.daos.FileDetailDao
import com.example.pacientrecord.localdb.daos.FilePatientDao
import com.example.pacientrecord.localdb.daos.FilePatientDetailDao
import com.example.pacientrecord.localdb.models.FileDetail
import com.example.pacientrecord.localdb.models.FilePatient
import com.example.pacientrecord.localdb.models.FilePatientDetail

@Database(entities = [FilePatient::class, FileDetail::class, FilePatientDetail::class], version = 8)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun filePatientDao() : FilePatientDao
    abstract fun fileDetailDao() : FileDetailDao
    abstract fun filePatientDetailDao() : FilePatientDetailDao
}