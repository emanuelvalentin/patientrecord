package com.example.pacientrecord.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.pacientrecord.Communication
import com.example.pacientrecord.R
import com.example.pacientrecord.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_register.*
import java.io.OutputStreamWriter
import java.lang.Thread.sleep
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.concurrent.thread

class RegisterFragment : Fragment() {

    companion object {
        fun newInstance(): RegisterFragment =
            RegisterFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_register.setOnClickListener {
            input_errorView_register.text = ""

            if(first_name.text.isNullOrEmpty() || last_name.text.isNullOrEmpty() || cnp_field.text.isNullOrEmpty() ||
                password_field.text.isNullOrEmpty() || re_enter_password.text.isNullOrEmpty() ) {

                input_errorView_register.text="Eroare: Toate campurile trebuie completate!"
                return@setOnClickListener
            }

            if (password_field.text.toString() != re_enter_password.text.toString()) {
                input_errorView_register.text="Eroare: Parolele nu coincid!"
                return@setOnClickListener
            }

            thread {
                    var address = "http://" + Communication.domain + "/Register.php"

                    var data = URLEncoder.encode(
                        "nume",
                        "utf-8"
                    ) + "=" + URLEncoder.encode(first_name.text.toString(), "utf-8") + "&" +
                            URLEncoder.encode(
                                "prenume",
                                "utf-8"
                            ) + "=" + URLEncoder.encode(last_name.text.toString(), "utf-8") + "&" +
                            URLEncoder.encode(
                                "cnp",
                                "utf-8"
                            ) + "=" + URLEncoder.encode(cnp_field.text.toString(), "utf-8") + "&" +
                            URLEncoder.encode(
                                "pass",
                                "utf-8"
                            ) + "=" + URLEncoder.encode(password_field.text.toString(), "utf-8")
                    var url = URL(address)

try {
    var conn = url.openConnection()
    conn.doOutput = true
    var outputStream = OutputStreamWriter(conn.getOutputStream())
    outputStream.write(data)
    outputStream.flush()
    outputStream.close()
    var scanner= Scanner(conn.getInputStream())
    var line= scanner.nextLine()
if(line.compareTo("Ok",true)==0)
    activity?.runOnUiThread {
        Toast.makeText(activity, "Register succesfully!", Toast.LENGTH_LONG).show()
        sleep(1000)
        activity?.runOnUiThread {
            activity?.startActivity(
                Intent(
                    activity,
                    MainActivity::class.java
                )
            )
            activity?.finish()
        }
    }
else
    Toast.makeText(activity, line, Toast.LENGTH_LONG).show()
}catch(E:Exception )
{
    Toast.makeText(activity, E.message, Toast.LENGTH_LONG).show()
}
                    }

        }

    }

}
