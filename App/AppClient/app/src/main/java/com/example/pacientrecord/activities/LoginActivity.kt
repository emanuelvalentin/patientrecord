package com.example.pacientrecord.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pacientrecord.R
import com.example.pacientrecord.fragments.LoginFragment
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if ( login_fragments_container != null) {

            if (savedInstanceState != null) {
                return
            }
            val loginFragment =
                LoginFragment.newInstance()

            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.login_fragments_container, loginFragment)
            transaction.addToBackStack("loginFragment")
            transaction.commit()
        }
    }


}
