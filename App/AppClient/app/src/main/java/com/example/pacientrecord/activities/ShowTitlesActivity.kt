package com.example.pacientrecord.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pacientrecord.R
import com.example.pacientrecord.adapters.ItemTitleViewAdapter
import com.example.pacientrecord.data.DataTitle
import kotlinx.android.synthetic.main.activity_show_titles.*

class ShowTitlesActivity : AppCompatActivity() {

    private fun initRecycleView() {
        recycle_titles.adapter = ItemTitleViewAdapter(this, DataTitle.data())
        recycle_titles.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_titles)
        initRecycleView()
    }


}
