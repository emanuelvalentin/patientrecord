package com.example.pacientrecord.localdb.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.pacientrecord.localdb.models.FilePatient

@Dao
interface FilePatientDao {

    @Insert
    fun insert(filePatient : FilePatient) : Long

    @Query("SELECT id FROM filePatient WHERE userId=:userId")
    fun selectAllIdsForUser(userId: Long) : List<Long>

}