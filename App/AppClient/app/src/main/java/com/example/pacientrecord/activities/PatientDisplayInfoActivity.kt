package com.example.pacientrecord.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pacientrecord.R
import com.example.pacientrecord.fragments.PatientUPUFragment

class PatientRecordsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_display_info)

        val upuFragment = PatientUPUFragment.newInstance()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.patient_records_container, upuFragment)
        transaction.commit()
    }
}
