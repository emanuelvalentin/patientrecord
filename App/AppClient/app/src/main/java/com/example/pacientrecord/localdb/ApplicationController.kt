package com.example.pacientrecord.localdb

import android.app.Application
import androidx.room.Room

class ApplicationController : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this

        appDatabase = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "db_patient_record"
        ).fallbackToDestructiveMigration().build()
    }

    companion object {
        var instance: ApplicationController? = null
            private set

        var appDatabase: AppDatabase? = null
            private set

    }
}