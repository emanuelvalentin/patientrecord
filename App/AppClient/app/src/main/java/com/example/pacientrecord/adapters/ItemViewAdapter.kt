package com.example.pacientrecord.adapters

import android.content.Context
import android.graphics.Color

import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.pacientrecord.R
import com.example.pacientrecord.data.Data
import com.example.pacientrecord.data.DataTitle
import org.json.JSONArray

class ItemViewAdapter(context: Context, data: ArrayList<Data>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var data: ArrayList<Data>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var inflater = LayoutInflater.from(parent.context)
        if (viewType == 0) {
            return TextHolder(
                inflater.inflate(
                    R.layout.item_text_view,
                    parent,
                    false
                )
            )
        }
        if (viewType == 4) {
            return DualCheckboxHolder(
                inflater.inflate(
                    R.layout.item_dual_check,
                    parent,
                    false
                )
            )
        }
        //   if(viewType==2)
        //   {
        //     return titleTextHolder(inflater.inflate(R.layout.item_text_view,parent,false))
        //   }
        if (viewType == 1) {
            return EditTextHolder(
                inflater.inflate(
                    R.layout.item_edit_text,
                    parent,
                    false
                )
            )
        }
        if (viewType == 2) {
            return checkBoxHolder(
                inflater.inflate(
                    R.layout.item_check_box,
                    parent,
                    false
                )
            )
        }
        if (viewType == 3) {
            return RadioButonHolder(
                inflater.inflate(
                    R.layout.item_radio_button,
                    parent,
                    false
                )
            )
        }

        return TextHolder(
            inflater.inflate(
                R.layout.item_text_view,
                parent,
                false
            )
        )
    }

    init {
        this.data = data
    }

    override fun getItemViewType(position: Int): Int {

        return data[position].type

    }

    override fun getItemCount(): Int {
        return data.size
    }

    public fun update(data: ArrayList<Data>)
    {

        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var Data = data[position]
        var type = Data.type
        if (type == 0) {
            (holder as TextHolder).text.text = data[position].content
            holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
            holder.text.setTextColor(Color.parseColor(Data.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)
            //readonly
            return
        }

        if (type == 1) {
            (holder as EditTextHolder).text.setText(Data.content)
            holder.id = position
            holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
            holder.text.setTextColor(Color.parseColor(Data.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)

            holder.text.setOnFocusChangeListener { v, hasFocus ->
                run {
                    if (hasFocus == true) {
                        Data as com.example.pacientrecord.data.EditText
                        holder.text.setTextColor(Color.parseColor(Data.FocusColor))
                        holder.text.setBackgroundColor(Color.parseColor(Data.FocusBackground))
                    } else {
                        holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
                        holder.text.setTextColor(Color.parseColor(Data.textColor))
                        Data.content = holder.text.text.toString()
                    }
                }
            }




            return
        }
        if (type == 2) {
            (holder as checkBoxHolder).text.text = data[position].content
            holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
            holder.text.setTextColor(Color.parseColor(Data.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)
            holder.text.isChecked = (Data as com.example.pacientrecord.data.CheckBox).isChecked
            holder.text.setOnClickListener {
                Data.isChecked = holder.text.isChecked
            }
            return
        }

        if (type == 3) {
            (holder as RadioButonHolder).text.text = data[position].content
            holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
            holder.text.setTextColor(Color.parseColor(Data.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)


            return
        }
        if (type == 4) {
            (holder as DualCheckboxHolder).text.text = data[position].content
            holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
            holder.text.setTextColor(Color.parseColor(Data.textColor))
            holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)
            holder.left.isChecked = (Data as com.example.pacientrecord.data.Dual).left
            holder.right.isChecked = Data.right
            holder.left.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)
            holder.right.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)
            holder.left.setOnClickListener {
                Data.left = holder.left.isChecked
            }
            holder.right.setOnClickListener {
                Data.right = holder.right.isChecked
            }
            var width = holder.right.width
            return
        }

        (holder as TextHolder).text.text = data[position].content
        holder.text.setBackgroundColor(Color.parseColor(Data.bgColor))
        holder.text.setTextColor(Color.parseColor(Data.textColor))
        holder.text.setTextSize(TypedValue.COMPLEX_UNIT_SP, Data.textsize)
    }

    fun save(): String {
        var index = 0
        var string = JSONArray()
        for (Data in data)
            string.put(Data.toString())
        return string.toString()
    }

    fun load(string: String) {
        // json= JSONObject(string)
    }

    fun getContent() : ArrayList<Data> {
        return data
    }

}

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)

class EditTextHolder(item: View) : RecyclerView.ViewHolder(item) {
    var text: EditText = item.findViewById(R.id.editTextItem)
    var id = -1
}

class checkBoxHolder(item: View) : RecyclerView.ViewHolder(item) {
    var text: CheckBox = item.findViewById(R.id.checkBoxItem)
    var id = -1
}

class TextHolder(item: View) : RecyclerView.ViewHolder(item) {
    var text: TextView = item.findViewById(R.id.textViewItem)
    var id = -1
}

class RadioButonHolder(item: View) : RecyclerView.ViewHolder(item) {
    var text: RadioButton = item.findViewById(R.id.radioButtonItem)
    var id = -1
}

class DualCheckboxHolder(item: View) : RecyclerView.ViewHolder(item) {
    var text: TextView = item.findViewById(R.id.info)
    var left: CheckBox = item.findViewById(R.id.left)
    var right: CheckBox = item.findViewById(R.id.right)
    var id = -1
}