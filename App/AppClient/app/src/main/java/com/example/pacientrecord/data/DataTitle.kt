package com.example.pacientrecord.data

open class DataTitle(var title: String = "", var subtitle: String = "", var id: Long =0) {
    companion object {
        fun data(): ArrayList<DataTitle> {
            return ArrayList<DataTitle>().apply {
                add(DataTitle(title = "Title", subtitle = "Subtitle"))
                add(DataTitle(title = "Title", subtitle = "Subtitle"))
                add(DataTitle(title = "Title", subtitle = "Subtitle"))
                add(DataTitle(title = "Title", subtitle = "Subtitle"))
                add(DataTitle(title = "Title", subtitle = "Subtitle"))
                add(DataTitle(title = "Title", subtitle = "Subtitle"))
            }
        }
    }

}